-------------------------------------------------------
|  Prédiction du Prix du Bitcoin avec un Modèle LSTM  |
-------------------------------------------------------

Description:

Ce projet utilise un modèle LSTM (Long Short-Term Memory) pour prédire le prix du Bitcoin et de l,'Ethereum.
Le modèle est entraîné sur des données historiques du prix du Bitcoin et de l'Ethereum, disponibles via l'API de Yahoo Finance, en utilisant la bibliothèque `yfinance`.

Prérequis:

Pour exécuter ce notebook, vous aurez besoin de Python 3.x ainsi que des bibliothèques suivantes :
- Pandas
- Keras (partie de TensorFlow 2.x)
- Matplotlib
- Numpy
- TensorFlow 2.x
- Scikit-learn
- YFinance
- Math

Il est recommandé d'utiliser un environnement virtuel pour gérer ces dépendances.

Installation des Dépendances:

Vous pouvez installer toutes les dépendances requises en exécutant :

 « !pip install pandas keras matplotlib numpy tensorflow scikit-learn yfinance »

Exécution du Notebook:

1. Assurez-vous que toutes les dépendances sont installées.
2. Ouvrez le notebook dans Google Colaboratory.
3. Exécutez les cellules dans l'ordre pour entraîner le modèle et visualiser les prédictions.

Structure du Projet:

Le notebook inclut les sections suivantes :

- Chargement et préparation des données
- Construction du modèle LSTM
- Entraînement du modèle
- Évaluation et visualisation des performances du modèle

Données:

Les données sont téléchargées en temps réel via `yfinance`. Aucune action supplémentaire n'est requise pour les obtenir.



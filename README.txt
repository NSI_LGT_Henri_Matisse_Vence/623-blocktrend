-------------------------------------------------------
|  Pr�diction du Prix du Bitcoin avec un Mod�le LSTM  |
-------------------------------------------------------

Description:

Ce projet utilise un mod�le LSTM (Long Short-Term Memory) pour pr�dire le prix du Bitcoin et de l,'Ethereum.
Le mod�le est entra�n� sur des donn�es historiques du prix du Bitcoin et de l'Ethereum, disponibles via l'API de Yahoo Finance, en utilisant la biblioth�que `yfinance`.

Pr�requis:

Pour ex�cuter ce notebook, vous aurez besoin de Python 3.x ainsi que des biblioth�ques suivantes :
- Pandas
- Keras (partie de TensorFlow 2.x)
- Matplotlib
- Numpy
- TensorFlow 2.x
- Scikit-learn
- YFinance
- Math

Il est recommand� d'utiliser un environnement virtuel pour g�rer ces d�pendances.

Installation des D�pendances:

Vous pouvez installer toutes les d�pendances requises en ex�cutant :

 ��!pip install pandas keras matplotlib numpy tensorflow scikit-learn yfinance��

Ex�cution du Notebook:

1. Assurez-vous que toutes les d�pendances sont install�es.
2. Ouvrez le notebook dans Google Colaboratory.
3. Ex�cutez les cellules dans l'ordre pour entra�ner le mod�le et visualiser les pr�dictions.

Structure du Projet:

Le notebook inclut les sections suivantes :

- Chargement et pr�paration des donn�es
- Construction du mod�le LSTM
- Entra�nement du mod�le
- �valuation et visualisation des performances du mod�le

Donn�es:

Les donn�es sont t�l�charg�es en temps r�el via `yfinance`. Aucune action suppl�mentaire n'est requise pour les obtenir.


